'use strict'

const cron = require('node-cron');

var Article = require('../models/article');
var http = require('./axios');

var moment = require('moment');

var mongoose = require('mongoose');
var db = mongoose.connection;

var cont = 0;

class GetData {
    static async getData() {
        const response = (await http.get('/')).data;

        if (cont > 0) {
            db.collection('articles').drop();
        }

        var aux = response.hits;
        for (const newArticle of aux) {
            // console.log(newArticle.created_at);
    
            var body = newArticle;

            var date = new Date(body.created_at);
            var myDate = moment(date, 'YYYYMMDD').format('LT');

            var article = new Article({
                created_at: myDate,
                title: body.title,
                url: body.url,
                author: body.author,
                story_title: body.story_title,
                story_url: body.story_url
            });

            // Para guardarlo
            await article.save((err, saveArticle) => {

                let res = {
                    ok: null,
                    status: null,
                    mensaje: '',
                    errors: null,
                    article: null
                };
        
                // 400 bad request
                if (err) {
                    return res = {
                        ok: false,
                        status: 400,
                        mensaje: 'Error al guardar artículo',
                        errors: err
                    };
                }

                // 201 recurso creado
                return res = {
                    ok: true,
                    status: 201,
                    article: saveArticle
                };

            });
        }
        cont++;
    }
}

if (cont == 0) {
    GetData.getData();
};

cron.schedule('0 * * * *', () => {
    GetData.getData();
});

var controller = {

    getArticles: (req, res) => {

        var query = Article.find({});

        // Find
        query.sort('-_id').exec((err, articles) => {

            if(err){
                return res.status(500).send({
                    status: 'error',
                    message: 'Error al devolver los articulos !!!'
                });
            }

            if(!articles){
                return res.status(404).send({
                    status: 'error',
                    message: 'No hay articulos para mostrar !!!'
                });
            }

            return res.status(200).send({
                status: 'success',
                articles
            });

        });
    },

    delete: (req, res) => {
        // Recoger el id de la url
        var articleId = req.params.id;

        // Find and delete
        Article.findOneAndDelete({_id: articleId}, (err, articleRemoved) => {
            if(err){
                return res.status(500).send({
                    status: 'error',
                    message: 'Error al borrar !!!'
                });
            }

            if(!articleRemoved){
                return res.status(404).send({
                    status: 'error',
                    message: 'No se ha borrado el articulo, posiblemente no exista !!!'
                });
            }

            return res.status(200).send({
                status: 'success',
                article: articleRemoved
            });

        }); 
    },

};  // end controller

module.exports = controller;