'use strict'

var express = require('express');
var ArticleController = require('../controllers/article');

var router = express.Router();

router.get('/articles', ArticleController.getArticles);
router.delete('/article/:id', ArticleController.delete);

module.exports = router;